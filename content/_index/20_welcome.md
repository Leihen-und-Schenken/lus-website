+++
fragment = "item"
weight = "20"
align = "right"

title = "Hallo — schön, dass Du da bist!"

[asset]
icon = "fas fa-gift"
+++

Einige von uns haben zu viel — andere zu wenig :-(

Vielleicht können wir dies durch Leihen und Schenken etwas ausgleichen!

Wir freuen uns über regen Waren-Austausch!

Als kleinen Appetitmacher möchten wir Euch folgenden Link empfehlen:

[30-Tage-Minimalisten-Spiel](https://www.theminimalists.com/game/)

Nutzt doch gerne diese Plattform für diese Herausforderung!
