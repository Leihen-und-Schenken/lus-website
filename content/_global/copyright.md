+++
fragment = "copyright"
weight = 1250
background = "dark"

attribution = true
copyright = "© 2022 magicfelix, Henrich von Lucke"
+++
