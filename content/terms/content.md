+++
fragment = "content"
weight = 100
title = "AGB"
background = "white"
+++

Bei dieser Web-Seite handelt es sich um ein privates, nicht-kommerzielles und nicht-gewerbliches Angebot
 Der Betreiber dieser Seite haftet nicht bzw. leistet keine Gewähr für die Verfügbarkeit der Seite oder die Verfügbarkeit der auf der Seite aufgelisteten Artikel, ebensowenig wir für deren Funktionsfähigkeit oder allgemein deren Zustand.

Das Einstellen von Artikeln oder auch beschreibenden Texten / Kommentaren mit rassistischem, sexistischem, antisemitischem, Gewalt-verherrlichendem oder sonstwie menschenfeindlichem Inhalt ist nicht erlaubt.
Der Administrator dieser Seite wird solche Inhalte bei Entdecken ohne Rücksprache löschen.

Ein Nutzer, der einen Artikel als zu Verschenken oder zu Verleihen einstellt, ist nicht verpflichtet, den entsprechenden Artikel an einen anfragenden anderen Nutzer zu verschenken oder zu verleihen.

Ein Nutzer, der einen Artikel als Geschenk von einem anderen Nutzer erhält, ist nicht verpflichtet, diesen Artikel über diese Plattform weiter zu verleihen oder zu verschenken.

Ein Nutzer, der einen Artikel als Leihgabe von einem anderen Nutzer erhält, ist verpflichtet, diesen Artikel zum vereinbarten Zeitraum an den Verleiher zurückzugeben.
Bei Verlust und Beschädigung eines geliehenen Artikels ist der Entleiher verpflichtet, den Artikel in angemessener Form dem Verleiher zu ersetzen bzw. zu erstatten.
Der Betreiber dieser Plattform übernimmt gebenüber einem Verleiher keine Haftung bei Verlust oder Beschädigung eines entliehenen Artikels und leistet somit keine Erstattung.

Der Administrator dieser Seite behält sich das Recht vor, Nutzer dieser Plattform bei ernsthaften Verstößen gegen diese Nutzungsregeln von der Anmeldung an dieser Plattform auszuschließen.
